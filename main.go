package main

import (
	"context"
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	// _ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gopkg.in/validator.v2"
)

type Db_conf struct {
	Db       string
	User     string
	Password string
	Host     string
}

// DB connection
func mysql_conn() *sql.DB {
	file, _ := os.Open("config/conf.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	Db_conf := Db_conf{}
	err := decoder.Decode(&Db_conf)
	if err != nil {
		fmt.Println("error:", err)
	}
	cfg := mysql.Config{
		User:   Db_conf.User,
		Passwd: Db_conf.Password,
		Net:    "tcp",
		Addr:   Db_conf.Host,
		DBName: Db_conf.Db,
	}
	db, err := sql.Open("mysql", cfg.FormatDSN())
	// db, err := sql.Open("mysql", "airflow:Sanoj!23@(127.0.0.1:3306)/test_go_sql?parseTime=true")
	// // inline method
	if err != nil {
		log.Fatal(err)
	}
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	// fmt.Println("Mysql connected")
	return db
}

type Users struct {
	Name     string `json:"name" validate:"nonzero"`
	Username string `json:"username" validate:"nonzero"`
	Password string `json:"password" validate:"nonzero,min=8,max=12,regexp=^[a-zA-Z0-9]*$"`
}

// REGISTRATION NEW USER
func resgisterFunc(c echo.Context) error {
	// userReg := Users{} (1)
	var userReg Users // (2)   1 or 2 equals
	defer c.Request().Body.Close()
	reqBody, err := io.ReadAll(c.Request().Body)
	if err != nil {
		log.Printf("Failed reading Body : %s\n", err)
		return c.String(http.StatusInternalServerError, "Internal server error")
	}
	err = json.Unmarshal(reqBody, &userReg)
	if err != nil {
		log.Printf("Unmarshel error : %s\n", err)
		return c.String(http.StatusInternalServerError, "Internal server error")
	}

	userRegValuePtr := reflect.ValueOf(&userReg)
	userRegValue := userRegValuePtr.Elem()

	for i := 0; i < userRegValue.NumField(); i++ {
		field := userRegValue.Field(i)
		if field.Type() != reflect.TypeOf("") {
			continue
		}
		str := field.Interface().(string)
		str = strings.TrimSpace(str)
		field.SetString(str)
	}

	if errs := validator.Validate(userReg); errs != nil {

		log.Println(errs)
		return c.String(http.StatusNotFound, "Invalid content")
	}

	if check_exists("SELECT USERID FROM TODO_USERS WHERE USERNAME=?", userReg.Username) {
		return c.String(http.StatusBadRequest, "Username Already exist")
	}
	userid := uuid.New().String()
	createdAt := time.Now()
	h_Md5 := md5.New()
	passHash := string(hex.EncodeToString(h_Md5.Sum([]byte(userReg.Password))[:]))

	if insert_data("TODO_USERS (USERID,NAME,USERNAME,PASSWORD,CREATED_AT) VALUES (?, ?, ?, ?, ?)", userid, userReg.Name, userReg.Username, passHash, createdAt) {
		return c.String(http.StatusInternalServerError, "Internal server error")
	}
	return c.JSON(http.StatusOK, "Success")

}

// INSERT INTO DATABASE QUERY FUNCTION
func insert_data(query string, args ...interface{}) bool {
	db := mysql_conn()
	defer db.Close()
	query = fmt.Sprintf("INSERT INTO %s", query)
	result, err := db.Exec(query, args...)
	if err != nil {
		log.Println(err)
		return true
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Println(err)
		return true
	}
	fmt.Println(id)

	return false
}

// CHECK ALREADY EXIST USER OR DATA IN DATABASE
func check_exists(query string, args ...interface{}) bool {
	db := mysql_conn()
	defer db.Close()
	var exists bool
	query = fmt.Sprintf("SELECT exists (%s)", query)
	err := db.QueryRow(query, args...).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		log.Printf("error checking if row exists '%s' %v", args, err)
	}
	return exists
}

type LoginResp struct {
	Apikey string `json:"api_key"`
}
type LoginUser struct {
	Username string `json:"username" validate:"nonzero"`
	Password string `json:"password" validate:"nonzero"`
}

// LOGIN AUTHENTICATION KEY GENERATION
func loginkeygen(args ...interface{}) (string, bool) {
	db := mysql_conn()
	defer db.Close()
	var userid string
	// query = fmt.Sprintf("INSERT INTO %s", query)
	query := "SELECT USERID FROM TODO_USERS WHERE USERNAME = ? AND PASSWORD= ?"
	if err := db.QueryRow(query, args...).Scan(&userid); err != nil {
		log.Println(err)
		return "", false
	}
	apikey := uuid.New().String()
	createdAt := time.Now()
	if insert_data("TODO_APIKEYS (USERID,API_KEY,CREATED_AT) VALUES (?, ?, ?)", userid, apikey, createdAt) {
		return "", false
	}
	return apikey, true

}

// LOGIN
func loginFunc(c echo.Context) error {
	loguser := LoginUser{}
	var logresp LoginResp
	reqBody, err := io.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()

	if err != nil {
		log.Printf("Failed reading Body : %s\n", err)
		return c.String(http.StatusInternalServerError, "")
	}
	err = json.Unmarshal(reqBody, &loguser)
	if err != nil {
		log.Printf("Unmarshel error : %s\n", err)
		return c.String(http.StatusInternalServerError, "")
	}
	// fmt.Printf("%v", loguser)
	loguser.Username = strings.TrimSpace(loguser.Username)
	loguser.Password = strings.TrimSpace(loguser.Password)
	// fmt.Printf("%v", loguser)
	if errs := validator.Validate(loguser); errs != nil {
		log.Println(errs)
		return c.String(http.StatusNotFound, "Invalid content")
	}
	// if check_exists("SELECT USERID FROM TODO_USERS WHERE USERNAME=?", loguser.Username) {
	h_Md5 := md5.New()
	passHash := string(hex.EncodeToString(h_Md5.Sum([]byte(loguser.Password))[:]))
	if apikey, err := loginkeygen(loguser.Username, passHash); err {
		// fmt.Println(apikey)
		logresp.Apikey = apikey
		return c.JSON(http.StatusOK, logresp)
	} else {
		return c.String(http.StatusOK, "Wrong Credentials")
	}

}

// middleware for api_key validation
func apikeyCheckMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		apikey := c.Request().Header.Get("X-API-KEY")
		if apikey != "" {
			db := mysql_conn()
			defer db.Close()
			var userid string
			query := "SELECT USERID FROM TODO_APIKEYS WHERE API_KEY = ?"
			errs := db.QueryRow(query, apikey).Scan(&userid)
			if errs != nil {
				log.Println(errs)
				return c.String(http.StatusUnauthorized, "User authentication Failed")
			}
			ctx := context.WithValue(c.Request().Context(), "api_key", apikey)
			ctx2 := context.WithValue(ctx, "userid", userid)
			newc := c.Request().WithContext(ctx2)
			c.SetRequest(newc)
			return next(c)
		}
		return c.String(http.StatusUnauthorized, "User authentication Failed")
	}
}

type Todo_item struct {
	Id     string `json:"id"`
	Item   string `json:"item" validate:"nonzero"`
	Status string `json:"status"`
}

// CREATING TODO ITEM
func createTodo(c echo.Context) error {
	// // fmt.Println(c.Request().Context())
	// fmt.Println(c.Request().Context().Value("api_key"))
	// fmt.Println(c.Request().Context().Value("userid"))

	var todoitem Todo_item
	reqBody, err := io.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		log.Printf("Failed reading Body : %s\n", err)
		return c.String(http.StatusInternalServerError, "")
	}
	err = json.Unmarshal(reqBody, &todoitem)
	if err != nil {
		log.Printf("Unmarshel error : %s\n", err)
		return c.String(http.StatusInternalServerError, "")
	}
	todoitem.Item = strings.TrimSpace(todoitem.Item)
	if errs := validator.Validate(todoitem); errs != nil {
		log.Println(errs)
		return c.String(http.StatusNotFound, "Invalid content")
	}
	createdAt := time.Now()
	if insert_data("TODO_LIST (USERID,ITEM,STATUS,CREATED_AT) VALUES (?, ?, ?, ?)", c.Request().Context().Value("userid"), todoitem.Item, "PENDING", createdAt) {
		return c.String(http.StatusInternalServerError, "some error")
	}
	return c.String(http.StatusOK, "Success")

}

// GET ALL TODO
func getTodoslist(c echo.Context) error {
	// fmt.Println(c.Request().Context().Value("userid"))
	db := mysql_conn()
	defer db.Close()
	rows, err := db.Query("SELECT ID, ITEM, STATUS FROM TODO_LIST WHERE USERID = ?", c.Request().Context().Value("userid"))
	if err != nil {
		log.Println(err)
		return c.String(http.StatusInternalServerError, "Invalid condent")
	}
	defer rows.Close()

	var todoItem []Todo_item
	for rows.Next() {
		var u Todo_item
		if err := rows.Scan(&u.Id, &u.Item, &u.Status); err != nil {
			log.Println(err)
			return c.String(http.StatusInternalServerError, "Invalid condent")
		}
		todoItem = append(todoItem, u)
	}
	// fmt.Println(todoItem)
	return c.JSON(http.StatusOK, todoItem)
}

// EDIT TODO ITEM
func updateTodo(c echo.Context) error {
	todoId := c.Param("todoid")
	log.Println("Todo id : ", todoId)
	if todoId == "" {
		log.Println("Bad request")
		return c.String(http.StatusBadRequest, "Invalid request")
	}
	if !check_exists("SELECT ITEM FROM TODO_LIST WHERE USERID =? AND ID=?", c.Request().Context().Value("userid"), todoId) {
		return c.String(http.StatusNotFound, "Item not found")
	}

	var todoitem Todo_item
	reqBody, err := io.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		log.Printf("Failed reading Body : %s\n", err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	err = json.Unmarshal(reqBody, &todoitem)
	if err != nil {
		log.Printf("Unmarshel error : %s\n", err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	todoitem.Item = strings.TrimSpace(todoitem.Item)
	if errs := validator.Validate(todoitem); errs != nil {
		log.Println(errs)
		return c.String(http.StatusNotFound, "Invalid request")
	}
	update_at := time.Now()
	db := mysql_conn()
	defer db.Close()

	query := `UPDATE TODO_LIST
			SET ITEM = ? ,LAST_UPDATED_AT = ?
			WHERE ID = ?;`
	_, err = db.Exec(query, todoitem.Item, update_at, todoId)
	if err != nil {
		log.Println(err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	return c.String(http.StatusOK, "Success")

}

type Todo_status struct {
	Status string `json:"status" validate:"nonzero"`
}

// UPDATE TODO STATUS
func updateTodoStatus(c echo.Context) error {
	todoId := c.Param("todoid")
	// log.Println("Todo id : ", todoId)
	if todoId == "" {
		log.Println("Bad request")
		return c.String(http.StatusBadRequest, "Invalid request")
	}
	if !check_exists("SELECT ITEM FROM TODO_LIST WHERE USERID =? AND ID=?", c.Request().Context().Value("userid"), todoId) {
		return c.String(http.StatusNotFound, "Item not found")
	}

	var todoitem Todo_status
	reqBody, err := io.ReadAll(c.Request().Body)
	defer c.Request().Body.Close()
	if err != nil {
		log.Printf("Failed reading Body : %s\n", err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	err = json.Unmarshal(reqBody, &todoitem)
	if err != nil {
		log.Printf("Unmarshel error : %s\n", err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	todoitem.Status = strings.TrimSpace(todoitem.Status)
	if errs := validator.Validate(todoitem); errs != nil {
		log.Println(errs)
		return c.String(http.StatusNotFound, "Invalid request")
	}
	if todoitem.Status != "PENDING" && todoitem.Status != "DONE" {
		return c.String(http.StatusNotFound, "Invalid request")
	}
	update_at := time.Now()
	db := mysql_conn()
	defer db.Close()

	query := `UPDATE TODO_LIST
			SET STATUS = ? ,LAST_UPDATED_AT = ?
			WHERE ID = ? AND USERID =?;`
	_, err = db.Exec(query, todoitem.Status, update_at, todoId, c.Request().Context().Value("userid"))
	if err != nil {
		log.Println(err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	return c.String(http.StatusOK, "Success")
}

// DELETE TODO
func deleteTodoItem(c echo.Context) error {
	todoId := c.Param("todoid")

	if todoId == "" {
		log.Println("Bad request")
		return c.String(http.StatusBadRequest, "Invalid request")
	}
	if !check_exists("SELECT ITEM FROM TODO_LIST WHERE USERID =? AND ID=?", c.Request().Context().Value("userid"), todoId) {
		return c.String(http.StatusNotFound, "Item not found")
	}
	db := mysql_conn()
	defer db.Close()

	query := `DELETE FROM TODO_LIST
			WHERE ID = ? AND USERID = ?;`
	_, err := db.Exec(query, todoId, c.Request().Context().Value("userid"))
	if err != nil {
		log.Println(err)
		return c.String(http.StatusInternalServerError, "Internal Server error")
	}
	return c.String(http.StatusOK, "Success")
}

func main() {
	fmt.Println("TODO API - Server Starting")

	e := echo.New()
	e.POST("/register", resgisterFunc)
	e.POST("/login", loginFunc)

	todosgroup := e.Group("/todos")
	todosgroup.Use(apikeyCheckMiddleware)
	todosgroup.GET("", getTodoslist)
	todosgroup.POST("", createTodo)
	todosgroup.PUT("/:todoid", updateTodo)
	todosgroup.DELETE("/:todoid", deleteTodoItem)
	todosgroup.PUT("/:todoid/status", updateTodoStatus)
	// todosgroup.PUT("")
	e.Start(":8000")
}
